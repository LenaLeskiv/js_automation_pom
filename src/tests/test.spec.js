const {pages} = require('./../po');

describe("Test suite", ()=>{
    beforeEach(async ()=>{
    await pages('main').open();
    await browser.maximizeWindow();
})
    it("Check page title", async()=>{
        const pageTitle = await browser.getTitle();
        console.log(pageTitle);
        expect(pageTitle).toEqual("EPAM | Software Engineering & Product Development Services");
    });

    it("Open left sidebar", async()=>{
        await browser.pause(1500);
        await pages('header').headerMenu.hamburgerMenu.click();
        await browser.pause(1500);
        await expect($('.os-content')).toBeDisplayed();

        const sidebarItem1 = await pages('main').sideMenu.item('services');
        await sidebarItem1.click(); 
        await expect($('.text')).toHaveText('Services');
        await $('.hamburger-menu-ui').click();
        await browser.pause(1500);

        const sidebarItem2 = await pages('main').sideMenu.item('industries');
        await sidebarItem2.click(); 
        await expect($('[gradient-text="Industries"]>.hamburger-menu__sub-list')).toBeDisplayed();
        await browser.pause(1500);
        await sidebarItem2.click();

        const sidebarItem3 = await pages('main').sideMenu.item('insights');
        await sidebarItem3.click(); 
        expect(await browser.getTitle()).toEqual('Discover our Latest Insights | EPAM');
        await browser.pause(1500);
        await pages('header').headerMenu.hamburgerMenu.click();
        await browser.pause(1500);

        const sidebarItem4 = await pages('main').sideMenu.item('about');
        await sidebarItem4.click(); 
        await expect($('.text')).toHaveText('About');
        await browser.pause(1500);
        await pages('header').headerMenu.hamburgerMenu.click();
        await browser.pause(1500);

        const sidebarItem5 = await pages('main').sideMenu.item('careers');
        await sidebarItem5.click(); 
        expect(await browser.getTitle()).toEqual('Explore Professional Growth Opportunities | EPAM Careers');
        await browser.pause(1500);

        await pages('header').headerMenu.logoEpam.click();
        await browser.pause(1500);
        expect(await browser.getTitle()).toEqual('EPAM | Software Engineering & Product Development Services');
        
        
    });
     
        it("Header Navigation Hover", async()=>{
            await browser.pause(1500);
            await pages('header').headerMenu.topNavigation.isDisplayed();
            await browser.pause(1500);
            //const topNavigationHover11 = await $('.top-navigation__row li:nth-child(1).top-navigation__item').moveTo();
            const topNavigationServices = await pages('header').topNavigation.element('services');
            const topNavigationServicesAttr = await pages('header').topNavigation.element('services').getAttribute('class'); //top-navigation__item epam
            console.log(topNavigationServicesAttr);
            await topNavigationServices.moveTo(); 
            await browser.pause(1500);
            const topNavigationServicesAfterAttr = await pages('header').topNavigation.element('services').getAttribute('class');//top-navigation__item epam js-opened
            console.log(topNavigationServicesAfterAttr);
            expect (topNavigationServicesAfterAttr).toEqual('top-navigation__item epam js-opened');
            if (topNavigationServicesAttr != topNavigationServicesAfterAttr)
            console.log('The Services drop-down is expanded');

            const topNavigationIndustries = await pages('header').topNavigation.element('industries');
            const topNavigationIndustriesAttr =  await topNavigationIndustries.getAttribute('class');
            console.log(topNavigationIndustriesAttr);
            await topNavigationIndustries.moveTo(); 
            await browser.pause(1500);
            const topNavigationIndustriesAfterAttr =  await topNavigationIndustries.getAttribute('class');
            console.log(topNavigationIndustriesAfterAttr);
            expect (topNavigationIndustriesAfterAttr).toEqual('top-navigation__item epam js-opened');
            if (topNavigationIndustriesAttr != topNavigationIndustriesAfterAttr)
            console.log('The Industries drop-down is expanded');
            
            const topNavigationInsights = await pages('header').topNavigation.element('insights');
            const topNavigationInsightsAttr =  await topNavigationInsights.getAttribute('class');
            console.log(topNavigationInsightsAttr);
            await topNavigationInsights.moveTo(); 
            await browser.pause(1500);
            const topNavigationInsightsAfterAttr =  await topNavigationIndustries.getAttribute('class');
            console.log(topNavigationIndustriesAfterAttr);
            expect (topNavigationInsightsAfterAttr).toEqual('top-navigation__item epam');
            if (topNavigationInsightsAttr == topNavigationInsightsAfterAttr)
            console.log('The Insights drop-down is not expanded, just highlighted in blue');

            const topNavigationAbout = await pages('header').topNavigation.element('about');
            const topNavigationAboutAttr =  await topNavigationAbout.getAttribute('class');
            console.log(topNavigationAboutAttr);
            await topNavigationAbout.moveTo(); 
            await browser.pause(1500);
            const topNavigationAboutAfterAttr =  await topNavigationAbout.getAttribute('class');
            console.log(topNavigationAboutAfterAttr);
            expect (topNavigationAboutAfterAttr).toEqual('top-navigation__item epam js-opened');
            if (topNavigationAboutAttr != topNavigationAboutAfterAttr)
            console.log('The About drop-down is expanded');

            const topNavigationCareers = await pages('header').topNavigation.element('careers');
            const topNavigationCareersAttr =  await topNavigationCareers.getAttribute('class');
            console.log(topNavigationCareersAttr);
            await topNavigationCareers.moveTo(); 
            await browser.pause(1500);
            const topNavigationCareersAfterAttr =  await topNavigationCareers.getAttribute('class');
            console.log(topNavigationCareersAfterAttr);
            expect (topNavigationCareersAfterAttr).toEqual('top-navigation__item epam js-opened');
            if (topNavigationCareersAttr != topNavigationCareersAfterAttr)
            console.log('The Careers drop-down is expanded');
        });

//         let steps = [
//             {name:'Services', index:0, selector:'[href="/services"]'},
//             {name:'Industries', index:1, selector:'[gradient-text="Industries"]>div'},
//             {name:'Insights', index:2, selector:'[href="/insights"]'},
//             {name:'About', index:3, selector:'[href="/about"]'},
//             {name:'Careers', index:4, selector:'[href="/careers"]'},
//             {name:'Contact us', index:5, selector:'.cta-button-ui'}
//         ];
//         for(let step of steps){
// it(`Click ${step.name}`, async()=>{
//     await browser.pause(1500);
//         await $('.hamburger-menu-ui').click();
//         await browser.pause(1500);
//         await expect($('.os-content')).toBeDisplayed();

//         const sidebarItem = (await mainPage.sideMenu.item())[step.index];
//         await sidebarItem.click(); 

//     //let link = [step.index];

    
//     })

//         }
        

    //     const links = await $$('.os-content a');
    //     await links.forEach(async (link) => {
    //     await link.click()
    // })
        
        //click on doctors list in side menu
        // const sideBar = await mainPage.sideMenu.this.rootEl.$(selectors[param]);
        // console.log(sideBar);
        // for (let param in sideBar){
        //     await sideBar.click();
        //     console.log(sideBar);
        // }


        // const doctorItem = await dashboardPage.sideMenu.item('doctors');
        // await doctorItem.click(); 
        // //click on add new doctor btn
        // await $('.specialization-types button.e-control').click();
        // //Add the new doctor
        // // await $('[name="Name"]').setValue('Mike');
        // // await $('[name="Name"]').waitForDisplayed(10000);
        // // await $('#DoctorMobile').setValue('11111111111111111111');
        // // await $('#DoctorMobile').waitForDisplayed(10000);
        // // await $('[name="Email"]').setValue('qq@gmail.com');
        // // await $('[name="Email"]').waitForDisplayed(10000);
        // // await $('[name="Education"]').setValue('accounter');
        // // await $('[name="Education"]').waitForDisplayed(10000);
        // // await $('.button-container button.e-primary').click();

        // await expect($('[cssclass="new-doctor-dialog"]')).not.toBeDisplayed();

        // await expect($('#Specialist_8').$('.name')).toHaveText('Dr. Mike');
        // await expect($('#Specialist_8').$('.education')).toHaveText('ACCOUNTER');
        // await expect($('#Specialist_8').$('.experience .specialization-text')).toHaveText('1+ years')
     
    

    // it("Close the modal window for adding doctor", async()=>{
    //     //click on doctors list in side menu
    //     const doctorItem = await dashboardPage.sideMenu.item('doctors');
    //     await doctorItem.click(); 
    //     await browser.pause(1500);
    //     //click on add new doctor btn
    //     await $('.specialization-types button.e-control').click();
    //     await browser.pause(1500);
    //     //check that modal window is displayed
    //     await expect($('[cssclass="new-doctor-dialog"]')).toBeDisplayed();
    //     await browser.pause(1500);
    //     await $('//button[text()="Cancel"]').click();
    //     await expect($('[cssclass="new-doctor-dialog"]')).not.toBeDisplayed();
    // })
})