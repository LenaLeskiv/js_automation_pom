
const {Header, SideMenuComponent, TopNavigationComponent} = require('./../components');

class BasePage {
    
    constructor (url) {
    this.url = url;
    this.header = new Header();
    this.sideMenu = new SideMenuComponent();
    this.topNavigation = new TopNavigationComponent
}

    open() {
        return browser.url(this.url);
    }
}

module.exports = BasePage;