const BasePage = require('./base.page');

class ServicesPage extends BasePage{
    
    constructor() {
        super('/services');
    }
    
}
module.exports = ServicesPage;