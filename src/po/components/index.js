const Header = require('../components/common/header.component');
const SideMenuComponent = require('../components/common/sidemenu.component');
const HeaderComponent = require('../components/common/header.component');
const TopNavigationComponent = require('../components/common/topnavigation.component');

module.exports = {
    Header,
    SideMenuComponent,
    HeaderComponent,
    TopNavigationComponent
}

