const BaseComponent = require('./base.component');

class HeaderComponent extends BaseComponent{

    constructor() {
        super('.header__content');
    }
    // get rootEl() {
    //     return $('.header__content');
    // }

    get hamburgerMenu() {
        return this.rootEl.$('.hamburger-menu-ui'); //.header__content .hamburger-menu-ui
    }

    get logoEpam() {
        return this.rootEl.$('.desktop-logo');  //.header__content .desktop-logo  .header__content .mobile-logo>.header__logo-light
    }

    get switchToggle(){
        return this.rootEl.$('>section');
    }

    get topNavigation(){
        return this.rootEl.$('.top-navigation-ui-23');
    }

    get contactBtn(){
        return this.rootEl.$('>a.cta-button-ui-23');
    }

    get locationSelector(){
        return this.rootEl.$('.location-selector-ui');
    }

    get headerSearch() {
        return this.rootEl.$('.header-search__button'); //.header__content .header-search__button
    }

}

module.exports = HeaderComponent;